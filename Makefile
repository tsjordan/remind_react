ARCHS_IOS = x86_64-apple-ios aarch64-apple-ios 
# i386-apple-ios armv7-apple-ios armv7s-apple-ios  
ARCHS_ANDROID = aarch64-linux-android armv7-linux-androideabi i686-linux-android
LIB=libremind.a

all: android_apk

ios: $(LIB) npm pod
	npx react-native run-ios

android: npm copyJNI

npm:
	npm install

android_apk: android
	cd android && ./gradlew assembleRelease

.PHONY: $(ARCHS_IOS)
$(ARCHS_IOS): %:
	cd remind && cargo build --target $@ --release

.PHONY: $(ARCHS_ANDROID)
$(ARCHS_ANDROID): %:
	cd remind && cargo build --target $@ --release

JNI_DIR = ./android/app/src/main/jniLibs
copyJNI: $(ARCHS_ANDROID)
	mkdir -p $(JNI_DIR)/x86/
	mkdir -p $(JNI_DIR)/arm64-v8a/
	mkdir -p $(JNI_DIR)/armeabi-v7a/
	cp ./remind/target/i686-linux-android/release/*.so      $(JNI_DIR)/x86/
	cp ./remind/target/aarch64-linux-android/release/*.so   $(JNI_DIR)/arm64-v8a/
	cp ./remind/target/armv7-linux-androideabi/release/*.so $(JNI_DIR)/armeabi-v7a/

$(LIB): $(ARCHS_IOS)
	cd remind && lipo -create -output $@ $(foreach arch,$(ARCHS_IOS),target/$(arch)/release/$(LIB))

pod:
	cd ios && pod install

clean: clean_ios clean_android
	cd remind && cargo clean
	npm cache clean --force

clean_android:
	rm -rf android/app/build

clean_ios:
	cd ios && xcodebuild clean
	cd ios && pod cache clean --all
