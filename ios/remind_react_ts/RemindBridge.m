//
//  RemindBridge.m
//  remind_react_ts
//
//  Created by Marek Kotewicz on 18/08/2017.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import <React/RCTBridgeModule.h>

@interface RCT_EXTERN_MODULE(RemindBridge, NSObject)

RCT_EXTERN_METHOD(exampleRegimen: (RCTPromiseResolveBlock)resolve reject:(RCTPromiseRejectBlock)reject);
RCT_EXTERN_METHOD(pushActivity:(NSString*)push_act_cmd resolve:(RCTPromiseResolveBlock)resolve reject:(RCTPromiseRejectBlock)reject);
RCT_EXTERN_METHOD(popActivity:(NSString*)pop_act_cmd resolve:(RCTPromiseResolveBlock)resolve reject:(RCTPromiseRejectBlock)reject);

@end
