//
//  RemindBridge.swift
//  remind_react_ts
//
//  Created by Marek Kotewicz on 18/08/2017.
//  Copyright © 2017 Facebook. All rights reserved.
//

import Foundation

@objc(RemindBridge)

class RemindBridge: NSObject {

	@objc func exampleRegimen(_ resolve: RCTPromiseResolveBlock, reject: RCTPromiseRejectBlock) -> Void {
		let result_rust_str = ExampleRegimen()
		let result_rust_str_ptr = rust_string_ptr(result_rust_str)
		let result = String.fromStringPtr(ptr: result_rust_str_ptr!.pointee)
		rust_string_ptr_destroy(result_rust_str_ptr)
		rust_string_destroy(result_rust_str)
		resolve(result)
	}

	@objc func pushActivity(_ push_act_cmd: String, resolve: RCTPromiseResolveBlock, reject: RCTPromiseRejectBlock) -> Void {
		var push_act_cmd_ptr = push_act_cmd.asPtr()
		let result_rust_str = PushActivity(&push_act_cmd_ptr)
		let result_rust_str_ptr = rust_string_ptr(result_rust_str)
		let result = String.fromStringPtr(ptr: result_rust_str_ptr!.pointee)
		rust_string_ptr_destroy(result_rust_str_ptr)
		rust_string_destroy(result_rust_str)
		resolve(result)
	}

	@objc func popActivity(_ pop_act_cmd: String, resolve: RCTPromiseResolveBlock, reject: RCTPromiseRejectBlock) -> Void {
		var pop_act_cmd_ptr = pop_act_cmd.asPtr()
		let result_rust_str = PopActivity(&pop_act_cmd_ptr)
		let result_rust_str_ptr = rust_string_ptr(result_rust_str)
		let result = String.fromStringPtr(ptr: result_rust_str_ptr!.pointee)
		rust_string_ptr_destroy(result_rust_str_ptr)
		rust_string_destroy(result_rust_str)
		resolve(result)
	}

}
