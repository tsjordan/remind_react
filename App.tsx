/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React, {useEffect, useState} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {createStackNavigator} from '@react-navigation/stack';
import Swipeable from 'react-native-swipeable';

import {
  ScrollView,
  StyleSheet,
  KeyboardAvoidingView,
  View,
  Text,
  Button,
  TextInput,
  TouchableHighlight,
  Platform,
} from 'react-native';

import {Colors} from 'react-native/Libraries/NewAppScreen';
declare const global: {HermesInternal: null | {}};

const Stack = createStackNavigator();

import {
  pushActivity,
  popActivity,
  loadRegimen,
  exampleRegimen,
} from './RemindBridge';

const HomeScreen = ({navigation}) => {
  return (
    <View
      style={{
        flex: 6,
        flexDirection: 'column',
        justifyContent: 'center',
      }}>
      <Button
        title="Edit Regimen"
        color="lightslategrey"
        onPress={() => navigation.push('Edit', {name: 'Edit'})}
      />
      <Button
        title="Add activities"
        color="lightseagreen"
        onPress={() => navigation.push('Add', {name: 'Add'})}
      />
      <Button
        title="Practice"
        onPress={() => navigation.navigate('Practice', {name: 'Jane'})}
      />
    </View>
  );
};

const Practice = ({_navigation}) => {
  console.log('Practice Start');
  console.log('initialize Regimen State:');
  const [regimen, setRegimen] = useState({});
  const [activity, setActivity] = useState({});
  const [showValue, setShowValue] = useState<bool>(false);
  const [tag, setTag] = useState<string>('');
  const [tagsList, setTagsList] = useState<list<string>>(['']);
  const validActivity = activity !== null ? true : false;
  console.log('activity:' + JSON.stringify(activity));
  console.log('validActivity:' + validActivity.toString());
  console.log('Regimen:' + JSON.stringify(regimen));
  const [time, setTime] = useState(Date.now());

  useEffect(() => {
    const interval = setInterval(() => setTime(Date.now()), 1000);
    return () => {
      clearInterval(interval);
    };
  }, []);

  const activityPop = (reg) => {
    popActivity(reg, tag).then((ret) => {
      setActivity(ret.activity);
      setRegimen(ret.regimen);
    });
  };
  // load regimen
  useEffect(() => {
    loadRegimen().then((reg) => {
      setTagsList(
        [].concat
          .apply(
            [],
            reg.activities.map((act, _index) => act.tags),
          )
          .sort()
          .filter((v, i, a) => a.indexOf(v) === i),
      );
      if (!activity) {
        activityPop(reg);
      }
    });
  }, [time]);

  useEffect(() => {
    loadRegimen().then((loadedReg) => {
      activityPop(loadedReg);
    });
  }, [tag]);

  const pushPop = (r_value) => {
    pushActivity(regimen, activity, r_value).then((reg) => {
      console.log(
        '\tPush activity "' +
          activity.title +
          '" with r_value=' +
          r_value.toString(),
      );
      activityPop(reg);
      setShowValue(false);
    });
  };

  const tagButtons = () => {
    const colors = ['#6FC75F', '#F5CF36', '#DE643C', '#AC2DCC', '#347DEB'];
    return (
      <View
        style={{
          flex: 2,
          flexWrap: 'wrap',
          flexDirection: 'row',
          justifyContent: 'flex-start',
        }}>
        {tagsList.map((t, index) => (
          <Button
            title={t}
            color={
              tag == t || tag == '' ? colors[index % colors.length] : 'gray'
            }
            onPress={() => {
              console.log('Set Tag');
              if (tag == t) {
                setTag('');
              } else {
                setTag(t.toLowerCase());
              }
              setShowValue(false);
            }}
          />
        ))}
      </View>
    );
  };
  console.log('Practice End');
  return (
    <View style={{flex: 8, justifyContent: 'space-around'}}>
      {tagButtons()}
      <View style={{flex: 6, justifyContent: 'space-around'}}>
        <Text style={styles.sectionTitle}>{activity && activity.title} </Text>
        <ScrollView>
          <Text style={styles.contentStyle}>
            {showValue
              ? activity && activity.value
              : activity && activity.content}
          </Text>
        </ScrollView>
        <Button
          title={showValue ? 'Hide Answer' : 'Show Answer'}
          color={showValue ? 'indigo' : 'cornflowerblue'}
          onPress={() => {
            console.log('onPress Value (act): ');
            setShowValue(!showValue);
            console.log('showAnswer: ' + showValue.toString());
          }}
          disabled={!validActivity}
        />
      </View>
      <View
        style={{
          flex: 0.75,
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
        <Button
          title="Failed it!"
          color="firebrick"
          onPress={() => {
            pushPop(0);
          }}
          disabled={!validActivity}
        />
        <Button
          title="Decent"
          color="goldenrod"
          onPress={async () => {
            pushPop(0.5);
          }}
          disabled={!validActivity}
        />
        <Button
          color="seagreen"
          title="Nailed it!"
          onPress={() => {
            pushPop(2.0);
          }}
          disabled={!validActivity}
        />
      </View>
    </View>
  );
};

const MyStack = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{title: 'Welcome'}}
        />
        <Stack.Screen name="Practice" component={Practice} />
        <Stack.Screen name="Add" component={Add} />
        <Stack.Screen name="Edit" component={Edit} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

const Edit = ({navigation}) => {
  const [regimen, setRegimen] = useState({activities: []});
  const [isSwiping, setIsSwiping] = useState(false);
  const [displayAct, setDisplayAct] = useState(['']);
  const [savable, setSavable] = useState(false);
  console.log('Enter "Edit"');
  useEffect(() => {
    loadRegimen().then((reg) => {
      console.log('load regimen');
      setRegimen(reg);
    });
  }, []);
  const activityButtons = () => {
    const rightButtons = [
      <TouchableHighlight>
        <Text
          style={{backgroundColor: 'firebrick', color: 'white', fontSize: 40}}>
          Delete
        </Text>
      </TouchableHighlight>,
    ];
    return (
      <ScrollView scrollEnabled={!isSwiping}>
        {regimen.activities.map((act, index) => (
          <Swipeable
            rightContent={rightButtons}
            onRightActionRelease={() => {
              let newAct = regimen.activities;
              const elimAct = newAct.splice(index, 1);
              console.log("removing '" + elimAct);
              const newReg = {activities: newAct};
              try {
                setRegimen(newReg);
                setSavable(true);
              } catch {
                console.log('Unable to set new Regimen');
              }
            }}
            onSwipeStart={() => setIsSwiping(true)}
            onSwipeRelease={() => setIsSwiping(false)}>
            <View>
              <TouchableHighlight
                onPress={() => {
                  console.log('Press' + act.title);
                  if (displayAct.includes(act.title)) {
                    setDisplayAct(displayAct.filter((x) => x !== act.title));
                  } else {
                    setDisplayAct(displayAct.concat(act.title));
                  }
                }}>
                <View
                  style={{
                    backgroundColor: '#DDDDDD',
                  }}>
                  <Text style={styles.titleText}>{act.title}</Text>
                </View>
              </TouchableHighlight>
              {displayAct.includes(act.title) ? (
                <View style={{flex: 1, height: 300}}>
                  {[
                    ['Description', 'content'],
                    ['Answer', 'value'],
                    ['Tags (comma-separated)', 'tags'],
                  ].map(([name, field], _idx) => (
                    <View style={{flex: 1}}>
                      <Text>{name + ':'}</Text>
                      <TextInput
                        style={[
                          {textAlignVertical: 'top', flex: 1},
                          styles.textInput,
                        ]}
                        multiline
                        value={
                          field === 'tags' ? act[field].toString() : act[field]
                        }
                        onChangeText={(text) => {
                          let newReg = Object.assign({}, regimen);
                          newReg.activities[index][field] =
                            field === 'tags'
                              ? text
                                  .split(',')
                                  .map((t) => t.trim().toLowerCase())
                              : text;
                          setRegimen(newReg);
                          setSavable(true);
                        }}
                      />
                    </View>
                  ))}
                  {act.practices.length
                    ? [act.practices[act.practices.length - 1]].map(
                        (lastPractice) => (
                          <>
                            <Text>
                              {'Interval: ' +
                                format_time(lastPractice.interval_ms)}
                            </Text>
                            <Text>
                              {'Due: ' +
                                new Date(
                                  Date.parse(lastPractice.date) +
                                    lastPractice.interval_ms,
                                ).toLocaleString()}
                            </Text>
                          </>
                        ),
                      )
                    : []}
                </View>
              ) : (
                []
              )}
            </View>
          </Swipeable>
        ))}
      </ScrollView>
    );
  };
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      keyboardVerticalOffset={Platform.OS === 'ios' ? 64 : 80}
      style={{
        flex: 6,
        flexDirection: 'column',
        justifyContent: 'center',
      }}>
      <Button
        title="load example regimen"
        onPress={() => {
          exampleRegimen().then((reg) => {
            setRegimen(reg);
            setSavable(true);
          });
        }}
      />
      {activityButtons()}
      <Button
        title="Save"
        onPress={() => {
          try {
            AsyncStorage.setItem('regimen', JSON.stringify(regimen));
            setSavable(false);
            navigation.goBack();
          } catch {
            //bad JSON
            console.log('Bad JSON');
          }
        }}
        disabled={!savable}
      />
    </KeyboardAvoidingView>
  );
};

const format_time = (ms) => {
  const msPerSecond = 1000;
  if (ms < msPerSecond) {
    return ms.toString() + ' ms';
  }
  let s = Math.floor(ms / msPerSecond);

  const secPerMinute = 60;
  if (s < secPerMinute) {
    return s.toString() + ' s';
  }
  let min = Math.floor(s / secPerMinute);
  s %= secPerMinute;

  const minPerHr = 60;
  if (min < minPerHr) {
    return min.toString() + ' min, ' + s.toString() + ' s';
  }
  let hr = Math.floor(min / minPerHr);
  min %= minPerHr;

  const hrPerDay = 24;
  if (hr < hrPerDay) {
    return (
      hr.toString() + ' hr, ' + min.toString() + ' min, ' + s.toString() + ' s'
    );
  }
  let day = Math.floor(hr / hrPerDay);
  hr %= hrPerDay;
  return (
    day.toString() +
    ' d, ' +
    hr.toString() +
    ' hr, ' +
    min.toString() +
    ' min, ' +
    s.toString() +
    ' s'
  );
};
const Add = ({navigation}) => {
  const [, setAct] = useState({});
  const [actName, setActName] = useState('');
  const [content, setContent] = useState('');
  const [value, setValue] = useState('');
  const [tag, setTag] = useState('');
  const [creatable, setCreatable] = useState(false);
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      keyboardVerticalOffset={Platform.OS === 'ios' ? 64 : 80}
      style={{
        flex: 6,
        flexDirection: 'column',
        justifyContent: 'center',
      }}>
      <Text>Activity Name</Text>
      <TextInput
        style={styles.textInput}
        onChangeText={(text) => {
          if (text == '') {
            setCreatable(false);
          } else {
            setCreatable(true);
          }
          setActName(text);
        }}
        value={actName}
      />
      <Text>Description</Text>
      <TextInput
        style={{
          flex: 2,
          height: 40,
          borderColor: 'gray',
          backgroundColor: 'white',
          borderWidth: 1,
        }}
        backgroundColor="white"
        onChangeText={(text) => setContent(text)}
        value={content}
        multiline={true}
        textAlignVertical="top"
      />
      <Text>Answer</Text>
      <TextInput
        style={{
          flex: 2,
          height: 40,
          borderColor: 'gray',
          backgroundColor: 'white',
          borderWidth: 1,
        }}
        onChangeText={(text) => setValue(text)}
        value={value}
        multiline={true}
        textAlignVertical="top"
      />
      <Text>Tags (Comma-separated)</Text>
      <TextInput
        style={styles.textInput}
        onChangeText={(text) => setTag(text.toLowerCase())}
        value={tag}
      />
      <Button
        title="Create Activity"
        disabled={!creatable}
        onPress={() => {
          var newAct = {
            title: actName,
            content: content,
            value: value,
            tags: tag.split(',').map((t) => t.trim()),
            practices: [],
          };
          setAct(newAct);
          console.log('Create Activity: ' + newAct.toString());
          loadRegimen().then((regimen) => {
            pushActivity(regimen, newAct, 1.0).then((reg) => {
              AsyncStorage.setItem('regimen', JSON.stringify(reg));
              setActName('');
              setTag('');
              setValue('');
              setContent('');
              setCreatable(false);
              navigation.goBack();
            });
          });
        }}
      />
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  textInput: {
    height: 40,
    borderColor: 'gray',
    backgroundColor: 'white',
    borderWidth: 1,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  titleText: {
    fontSize: 40,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  contentStyle: {
    marginTop: 8,
    fontSize: 20,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default MyStack;
