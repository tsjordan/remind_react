/**
 * This exposes the native RemindBridge module as a JS module. This has a
 * function 'show' which takes the following parameters:
 *
 * 1. String message: A string with the text to toast
 * 2. int duration: The duration of the toast. May be RemindBridge.SHORT or
 *    RemindBridge.LONG
 */

import { NativeModules } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
const RemindBridge = NativeModules.RemindBridge;

export const popActivity = async (regimen, tag) => {
  try {
    console.log('Enter PopActivity');
    console.log('       PopActivity pass regimen: ' + JSON.stringify(regimen));
    console.log('       PopActivity pass tag: ' + tag);
    const tags = tag === '' ? []: [tag.toLowerCase()];
    const popActCmd = {"regimen":regimen, "tags": tags};
    const json_return:string = await RemindBridge.popActivity(JSON.stringify(popActCmd));
    console.log('       PopActivity returned: ' + json_return);
    return JSON.parse(json_return);
  }catch(e) {
    return JSON.parse( '{"_W":{"title": "exception"}}');
  }
}

export const pushActivity = async (regimen, activity, r_value) => {
  try {
    console.log('Enter pushActivity');
    console.log('       pushActivity regimen: ' + JSON.stringify(regimen));
    console.log('       pushActivity activity: ' + JSON.stringify(activity));
    const push_act_cmd = {"regimen": regimen, "activity": activity, "r_value": r_value};
    const json_str_ret:string = await RemindBridge.pushActivity(JSON.stringify(push_act_cmd));
    console.log('       pushActivity returned: ' + json_str_ret);
    await AsyncStorage.setItem('regimen', json_str_ret)
    return JSON.parse(json_str_ret);
  }catch(e) {
    return JSON.parse( '{}');
  }
}

export const exampleRegimen:string = async () => {
  console.log('Enter exampleRegimen');
  try {
    console.log('   exampleRegimen, await fun');
    const regimen_json:string = await RemindBridge.exampleRegimen();
    console.log('   exampleRegimen, regimen_json:');
    console.log(typeof(regimen_json));
    console.log(regimen_json);
    console.log('   exampleRegimen, return in try');
    return JSON.parse(regimen_json);
  }catch(e) {
    console.log('   exampleRegimen, return in catch');
    return {};
  }
  console.log('   exampleRegimen, shouldn\'t be here');
}

export const loadRegimen = async () => {
  try {
    console.log('loading regimen');
    let reg_json = await AsyncStorage.getItem('regimen');
    if (reg_json == null){
      reg_json = await RemindBridge.exampleRegimen();
    }
    return JSON.parse(reg_json);
  }catch(e){
    console.log('CATCH loading regimen');
    console.error(e);
    return await RemindBridge.exampleRegimen();
  }
}
