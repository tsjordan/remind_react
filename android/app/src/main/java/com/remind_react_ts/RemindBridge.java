package com.remind_react_ts;

import android.app.Application;
import android.content.Context;
import com.facebook.react.PackageList;
import com.facebook.react.ReactApplication;
import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.soloader.SoLoader;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

public class RemindBridge extends ReactContextBaseJavaModule {
  static {
    System.loadLibrary("remind");
  }
  @Override
  public String getName() {
    return "RemindBridge";
  }

  public RemindBridge(ReactApplicationContext reactContext) {
    super(reactContext);
  }

  @ReactMethod
  public void popActivity(String pop_act_cmd, Promise promise) {
    promise.resolve(popActivity(pop_act_cmd));
  }

  @ReactMethod
  public void pushActivity(String push_act_cmd, Promise promise) {
    promise.resolve(pushActivity(push_act_cmd));
  }

  @ReactMethod
  public void exampleRegimen(Promise promise) {
    try {
      promise.resolve(exampleRegimen());
    } catch(Exception e) {
      promise.reject("exampleRegimen: ", e);
    }
  }

  // Functions in Rust Library
  private static native String exampleRegimen();
  private static native String popActivity(String pop_act_cmd);
  private static native String pushActivity(String push_act_cmd);
}
