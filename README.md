# ReMind

An to help remind you of things you want to memorize.

## Development

## Android

### Requirements

* Android NDK installed and the environment variables set
* Rust `cargo`. Add targets denoted in the pipeline

### Build

`. ./env.sh`
`make -j`

APK ends up in `./android/app/build/outputs/apk/release/`

You can install on your android simulator or on your USB-connected phone with:

* `npx react-native run-android` or
* `npx react-native run-android --variant=release`

## IOS

### Requirements

* Install XCode 
  * iPhone simulator from XCode (XCode>Preferences>Components(tab))
  * Command line tools for XCode (and link to XCode in XCode>Preferences>Locations(tab))
* Rust `cargo`. Add targets denoted in the pipeline
- Cacoa Pods (`sudo gem install cocoapods`)

### Build

- `make ios` to build rust library and get pods
- Open XCode Workspace (not project) created by React-Native in [./ios](./ios) and build
- Compile with the play button in upper left
- There may be an error in the RNCPanHandler, remove the offending lines 
  ```
  if (@available(iOS 13.4, *)) {
    bool enableTrackpadTwoFingerGesture = [RCTConvert BOOL:config[@"enableTrackpadTwoFingerGesture"]];
    if(enableTrackpadTwoFingerGesture){
      recognizer.allowedScrollTypesMask = UIScrollTypeMaskAll;
    }
  }
  ```
- `npx react-native run-ios`

